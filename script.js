document.addEventListener('DOMContentLoaded', async function() {
    const response = await fetch('https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json');
    const obj =  await response.json();
    document.getElementById('title').textContent = obj.name;
    const dataset = obj.data;

    const width = 900;
    const height = 400;
    const padding = 60;
    const { from_date, to_date } = obj;

    const svg = d3.select('.container').append('svg')
        .attr('class', 'svg')
        .attr('width', width)
        .attr('height', height);

    const xScale = d3.scaleTime()
        .domain([new Date(from_date), new Date(to_date)])
        .range([padding, width - padding]);

    const yScale = d3.scaleLinear()
        .domain([0, d3.max(dataset, d => d[1])])
        .range([height - padding, 0]);

    const xAxis = d3.axisBottom(xScale)
    const yAxis = d3.axisLeft(yScale);

    const rect = svg.selectAll('rect')
        .data(dataset)
        .enter()
        .append('rect')
        .attr('class', 'bar')
        .attr('x', (d, i) => xScale(new Date(d[0])))
        .attr('y', d => yScale(d[1]))
        .attr('width', (width - 2 * padding) / dataset.length)
        .attr('height', d => height - padding - yScale(d[1]))
        .attr('fill', 'cornflowerblue')
        .attr('data-date', d => d[0])
        .attr('data-gdp', d => d[1])

    svg.append("g")
        .attr("id", "x-axis")
        .attr("transform", `translate(${0}, ${height - padding})`)
        .call(xAxis);

    svg.append("g")
        .attr("id", "y-axis")
        .attr("transform", `translate(${padding}, 0)`)
        .call(yAxis);

    const rects = document.querySelectorAll('.bar');
    const tooltip = document.querySelector('#tooltip');
    rects.forEach(rect => {
        rect.onmouseover = () => {
            tooltip.setAttribute('data-date', rect.dataset.date);
            tooltip.setAttribute('data-gdp', rect.dataset.gdp);
            tooltip.innerHTML = `<p>${tooltip.dataset.date}</p><p>${tooltip.dataset.gdp}</p>`;
            tooltip.classList.add('tooltip-show');
            tooltip.style.left = `${rect.getAttribute('x')}px`;
        };
        rect.onmouseout = () => {
            tooltip.classList.remove('tooltip-show');
        };
    })
})